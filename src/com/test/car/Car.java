package com.test.car;

public class Car {

	private String model;
	
	private String color;
	
	private String currentSpeed;
	
	public Car(String whichModel, String whichColor) {
		this.model = whichModel;
		this.color = whichColor;
	}
	
	public String getModel() {
		return this.model;
	}
	
	public String getColor() {
		return this.color;
	}
	
	public void changeColor(String newColor) {
		this.color = newColor;
	}
	
	public void info() {
		System.out.println("Model: " +model+ "; Color: "+color+"; Current Speed: "+currentSpeed);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Car) {
			Car that = (Car) obj;
			
			return this.model.equals(that.model);
		} else
			return false;
	}
}
